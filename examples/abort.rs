use std::time::Duration;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let executor = jitterbug::Executor::new();

    foxtrot::block_on(executor.clone().run_with(async move {
        let handle = executor.spawn(async move {
            foxtrot::time::sleep(Duration::from_secs(5));
            println!("slept");
        });
        println!("Spawned task");

        handle.abort();
        println!("Canceled task");

        assert!(handle.await.is_err(), "Expected to abort task");
        println!("Awaited handle");
    }))??;

    Ok(())
}
