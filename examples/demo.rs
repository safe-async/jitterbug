use jitterbug::Executor;
use std::{
    sync::Arc,
    task::{Wake, Waker},
};

fn spawn(executor: &Executor) {
    println!("Spawning futures");

    let task1 = executor.spawn(async move {
        println!("Henlo from first spawn");
        "A"
    });

    let task2 = executor.spawn(async move {
        println!("Henlo from second spawn");
        "B"
    });

    let run2m = executor.clone();

    executor.spawn(async move {
        let res1 = task1.await;
        let res2 = task2.await;
        println!("Henlo from third spawn, {:?}, {:?}", res1, res2);

        let res3 = run2m
            .spawn(async move {
                println!("Henlo from inner spawn");
                "D"
            })
            .await;

        println!("Henlo again from third spawn, {:?}", res3);
    });
}

struct DummyWaker;
impl Wake for DummyWaker {
    fn wake(self: std::sync::Arc<Self>) {}
    fn wake_by_ref(self: &Arc<Self>) {}
}

fn main() {
    let executor = Executor::new();
    let runner = executor.clone().into_runner();

    // This creates 3 new tasks
    spawn(&executor);

    let waker: Waker = Arc::new(DummyWaker).into();

    while runner.any_woken() {
        println!("Ticking");
        runner.tick(waker.clone());
    }

    // This reclaims the first 3 tasks
    println!("Pruning");
    runner.prune();

    // This creates 3 new tasks
    spawn(&executor);
    while runner.any_woken() {
        println!("Ticking");
        runner.tick(waker.clone());
    }

    // This re-uses the 3 tasks created prior
    spawn(&executor);

    // This doesn't reclaim any tasks, since we've spawned 3 more futures
    println!("Pruning");
    runner.prune();

    while runner.any_woken() {
        println!("Ticking");
        runner.tick(waker.clone());

        // This reclaims tasks as their futures resolve on each tick
        println!("Pruning");
        runner.prune();
    }

    println!("Hewwo Mr Obama");
}
