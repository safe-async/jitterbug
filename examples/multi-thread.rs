use jitterbug::{oneshot, Executor, JoinError};

fn main() -> Result<(), JoinError> {
    let executor = Executor::new();

    for _ in 0..4 {
        let executor = executor.clone();
        std::thread::spawn(move || {
            let _ = executor.block_on(std::future::pending::<()>());
        });
    }

    let execu2r = executor.clone();
    executor.block_on(async move {
        let mut join_handles = Vec::new();
        for _ in 0..4 {
            join_handles.extend((0..502).flat_map(|i| {
                let (tx, rx) = oneshot();

                [
                    execu2r.spawn(async move {
                        let _ = tx.send(i);
                    }),
                    execu2r.spawn(async move {
                        if let Ok(t) = rx.await {
                            println!("{:?} - {}", std::thread::current().id(), t);
                        } else {
                            println!("{:?} - oops", std::thread::current().id());
                        }
                    }),
                ]
            }));
        }

        for handle in join_handles {
            handle.await?;
        }

        println!("Done waiting");

        Ok(()) as Result<(), JoinError>
    })?
}
