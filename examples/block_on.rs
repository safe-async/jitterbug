use jitterbug::{oneshot, Executor, JoinError};
use std::time::Duration;

fn main() -> Result<(), JoinError> {
    let executor = Executor::new();

    let execu2r = executor.clone();
    executor.block_on(async move {
        // Spawn a ton to invoke task pruning heuristic
        let join_handles = (0..1007)
            .map(|i| execu2r.spawn(async move { println!("{}", i) }))
            .collect::<Vec<_>>();

        for i in join_handles {
            i.await?;
        }

        Ok(()) as Result<(), JoinError>
    })??;

    let execu2r = executor.clone();
    std::thread::spawn(move || {
        let (tx, rx) = oneshot();

        execu2r.spawn(async move {
            println!("Started polling");
            let val = rx.await;
            println!("delayed print, {:?}", val);
        });

        // Delay a bit to invoke task pruning heuristic
        std::thread::sleep(Duration::from_secs(6));

        println!("sending meowdy");
        let _ = tx.send("meowdy");

        std::thread::sleep(Duration::from_secs(2));

        println!("stopping");
        execu2r.stop();
    });

    let res = executor.block_on(std::future::pending::<()>());
    println!("{:?}", res);
    Ok(())
}
