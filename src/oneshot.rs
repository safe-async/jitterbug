use std::{
    future::Future,
    pin::Pin,
    sync::{Arc, Mutex},
    task::{Context, Poll, Waker},
};

enum OneshotState<T> {
    New,
    Waker(Waker),
    Item(T),
    Closed,
}

pub struct Sender<T> {
    state: Arc<Mutex<OneshotState<T>>>,
}
pub struct Receiver<T> {
    state: Arc<Mutex<OneshotState<T>>>,
}

impl<T> OneshotState<T> {
    fn take(&mut self) -> Self {
        std::mem::replace(self, OneshotState::Closed)
    }
}

#[derive(Debug)]
pub struct Dropped;

pub fn oneshot<T>() -> (Sender<T>, Receiver<T>) {
    let state = Arc::new(Mutex::new(OneshotState::New));

    (
        Sender {
            state: Arc::clone(&state),
        },
        Receiver { state },
    )
}

impl<T> Sender<T> {
    pub fn send(self, item: T) -> Result<(), T> {
        self.send_borrowed(item)
    }

    pub(crate) fn send_borrowed(&self, item: T) -> Result<(), T> {
        let mut guard = self.state.lock().unwrap();

        match guard.take() {
            OneshotState::New => {
                *guard = OneshotState::Item(item);
                Ok(())
            }
            OneshotState::Waker(waker) => {
                *guard = OneshotState::Item(item);
                waker.wake();
                Ok(())
            }
            OneshotState::Item(_) | OneshotState::Closed => Err(item),
        }
    }
}

impl<T> Future for Receiver<T> {
    type Output = Result<T, Dropped>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut guard = self.state.lock().unwrap();

        match guard.take() {
            OneshotState::New => {
                *guard = OneshotState::Waker(cx.waker().clone());
                Poll::Pending
            }
            OneshotState::Waker(old_waker) => {
                if old_waker.will_wake(cx.waker()) {
                    *guard = OneshotState::Waker(old_waker);
                } else {
                    *guard = OneshotState::Waker(cx.waker().clone());
                }

                Poll::Pending
            }
            OneshotState::Item(item) => Poll::Ready(Ok(item)),
            OneshotState::Closed => Poll::Ready(Err(Dropped)),
        }
    }
}

impl<T> Drop for Sender<T> {
    fn drop(&mut self) {
        let mut guard = self.state.lock().unwrap();

        match guard.take() {
            OneshotState::Waker(waker) => waker.wake(),
            OneshotState::Item(item) => {
                *guard = OneshotState::Item(item);
            }
            OneshotState::New | OneshotState::Closed => {}
        }
    }
}

impl<T> Drop for Receiver<T> {
    fn drop(&mut self) {
        *self.state.lock().unwrap() = OneshotState::Closed;
    }
}

impl std::fmt::Display for Dropped {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Sender was dropped")
    }
}

impl std::error::Error for Dropped {}
