use std::{
    collections::{HashMap, VecDeque},
    future::Future,
    marker::PhantomData,
    pin::Pin,
    rc::Rc,
    sync::{
        atomic::{AtomicBool, AtomicU64, AtomicUsize, Ordering},
        Arc, Mutex, RwLock, Weak,
    },
    task::{Context, Poll, Wake, Waker},
    thread::{Thread, ThreadId},
    time::{Duration, Instant},
};

mod oneshot;
pub use oneshot::{oneshot, Dropped, Receiver, Sender};

const PRUNE_SPAWN_COUNT: u64 = 1000;
const PRUNE_DURATION: Duration = Duration::from_secs(5);
const UNPARK_DURATION: Duration = Duration::from_millis(50);

pub struct JoinHandle<T> {
    rx: Receiver<T>,
    abort: Arc<Sender<()>>,
}

pub struct AbortHandle {
    abort: Arc<Sender<()>>,
}

#[derive(Debug)]
pub struct JoinError;

impl std::fmt::Display for JoinError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Task panicked")
    }
}

impl std::error::Error for JoinError {}

impl AbortHandle {
    pub fn abort(&self) {
        let _ = self.abort.send_borrowed(());
    }
}

impl<T> JoinHandle<T> {
    pub fn abort(&self) {
        let _ = self.abort.send_borrowed(());
    }

    pub fn abort_handle(&self) -> AbortHandle {
        AbortHandle {
            abort: Arc::clone(&self.abort),
        }
    }
}

impl<T> Future for JoinHandle<T> {
    type Output = Result<T, JoinError>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        Pin::new(&mut self.rx)
            .poll(cx)
            .map(|res| res.map_err(|_| JoinError))
    }
}

type BoxFuture<'a, T> = Pin<Box<dyn Future<Output = T> + Send + 'a>>;

struct Task {
    task_id: u64,
    woken: AtomicBool,
    state: Mutex<Option<FutureState>>,
}

struct FutureState {
    future: BoxFuture<'static, ()>,
    abort: Option<Receiver<()>>,
}

impl Task {
    fn poll(self: Arc<Self>, executor: Weak<Mutex<ThreadState>>, waker: Waker) {
        self.woken.store(false, Ordering::Release);

        let res = std::panic::catch_unwind(|| {
            let safe_waker = Arc::new(SafeWaker {
                task: Arc::downgrade(&self),
                executor: Weak::clone(&executor),
                inner: waker,
            })
            .into();

            let mut context = Context::from_waker(&safe_waker);

            self.state.lock().unwrap().as_mut().map(|state| {
                if let Some(abort) = state.abort.as_mut() {
                    if let Poll::Ready(res) = Pin::new(abort).poll(&mut context) {
                        state.abort.take();
                        if res.is_ok() {
                            return Poll::Ready(());
                        }
                    }
                }

                state.future.as_mut().poll(&mut context)
            })
        });

        match res {
            Ok(opt) => {
                if opt.as_ref().map(|poll| poll.is_ready()).unwrap_or(false) {
                    self.state.lock().unwrap().take();

                    if let Some(executor) = Weak::upgrade(&executor) {
                        let mut inner = executor.lock().unwrap();
                        inner.pending.remove(&self.task_id);
                        inner.available.push_back(self);
                    }
                }
            }
            Err(_) => {
                if let Some(executor) = Weak::upgrade(&executor) {
                    let mut inner = executor.lock().unwrap();
                    // This will remove the last remaining Arc<Task> for this task, dropping after poll completes
                    inner.pending.remove(&self.task_id);
                }
            }
        }
    }

    fn allocate(task_id: u64) -> Arc<Self> {
        Arc::new(Self {
            task_id,
            woken: AtomicBool::new(false),
            state: Mutex::new(None),
        })
    }
}

struct SafeWaker {
    task: Weak<Task>,
    executor: Weak<Mutex<ThreadState>>,
    inner: Waker,
}

impl Wake for SafeWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        if let Some(task) = Weak::upgrade(&self.task) {
            if let Some(executor) = Weak::upgrade(&self.executor) {
                let mut inner = executor.lock().unwrap();

                if inner.pending.contains_key(&task.task_id)
                    && !task.woken.swap(true, Ordering::AcqRel)
                {
                    inner.wake(task);
                }
            }
        }

        self.inner.wake_by_ref();
    }
}

struct BlockOnWaker {
    thread: Thread,
}

impl Wake for BlockOnWaker {
    fn wake(self: Arc<Self>) {
        self.thread.unpark();
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.thread.unpark()
    }
}

struct Threads {
    threads: RwLock<HashMap<ThreadId, Arc<Mutex<ThreadState>>>>,
    last_spawn: AtomicUsize,
}

impl Threads {
    fn new() -> Self {
        Threads {
            threads: RwLock::new(HashMap::new()),
            last_spawn: AtomicUsize::new(0),
        }
    }
}

struct ThreadState {
    available: VecDeque<Arc<Task>>,
    woken: VecDeque<Arc<Task>>,
    pending: HashMap<u64, Arc<Task>>,
    handle: Thread,
    spawn_count: u64,
    prune_time: Instant,
    unpark_time: Instant,
    last_steal: Option<ThreadId>,
    waker: Option<Waker>,
}

impl ThreadState {
    fn new() -> Self {
        ThreadState {
            available: VecDeque::new(),
            woken: VecDeque::new(),
            pending: HashMap::new(),
            handle: std::thread::current(),
            spawn_count: 0,
            prune_time: Instant::now(),
            unpark_time: Instant::now(),
            last_steal: None,
            waker: None,
        }
    }

    fn steal_from(&mut self) -> Option<Vec<Arc<Task>>> {
        let split_point = (self.woken.len() - (self.woken.len() % 2)) / 2;

        if split_point > 0 {
            let v = self.woken.drain(split_point..).collect::<Vec<_>>();

            for task in &v {
                self.pending.remove(&task.task_id);
            }

            return Some(v);
        }

        None
    }

    fn pop_available_head(&mut self) -> Option<Arc<Task>> {
        self.available.pop_front()
    }

    fn heuristic_prune(&mut self) {
        if self.spawn_count > PRUNE_SPAWN_COUNT || self.prune_time + PRUNE_DURATION < Instant::now()
        {
            self.prune();
        }
    }

    fn should_heuristic_unpark(&mut self) -> bool {
        self.spawn_count % 10 == 0 || self.unpark_time + UNPARK_DURATION < Instant::now()
    }

    fn update_unpark(&mut self) {
        self.unpark_time = Instant::now();
    }

    fn prune(&mut self) {
        self.available = VecDeque::new();
        self.prune_time = Instant::now();
        self.spawn_count = 0;
    }

    fn wake(&mut self, task: Arc<Task>) {
        task.woken.store(true, Ordering::Release);
        self.woken.push_back(task);
    }

    fn spawn<T: Send + 'static>(
        &mut self,
        shared: &Arc<Inner>,
        future: impl Future<Output = T> + Send + 'static,
    ) -> JoinHandle<T> {
        let task = self
            .pop_available_head()
            .unwrap_or_else(|| Task::allocate(shared.next_task_id()));

        self.pending.insert(task.task_id, Arc::clone(&task));

        let (abort_tx, abort_rx) = oneshot();

        let (tx, rx) = oneshot();

        let state = FutureState {
            future: Box::pin(async move {
                let _ = tx.send(future.await);
            }),
            abort: Some(abort_rx),
        };
        *task.state.lock().unwrap() = Some(state);
        self.wake(task);
        self.spawn_count += 1;

        if self.should_heuristic_unpark() {
            if let Some(thread) = shared.parked.lock().unwrap().pop_front() {
                thread.unpark();
            }
            self.update_unpark();
        }

        JoinHandle {
            rx,
            abort: Arc::new(abort_tx),
        }
    }
}

struct Inner {
    task_id_counter: AtomicU64,
    stopping: AtomicBool,
    threads: Threads,
    parked: Mutex<VecDeque<Thread>>,
}

impl Inner {
    fn next_task_id(&self) -> u64 {
        self.task_id_counter.fetch_add(1, Ordering::Relaxed)
    }

    fn stop(&self) {
        self.stopping.store(true, Ordering::Release);
        let read_guard = self.threads.threads.read().unwrap();

        for state in read_guard.values() {
            if let Some(waker) = state.lock().unwrap().waker.take() {
                waker.wake();
            }
        }
    }
}

#[derive(Clone)]
pub struct Executor {
    inner: Arc<Inner>,
}

// SHOULD NOT IMPLEMENT CLONE !!!!!!!
// SHOULD NOT IMPLEMENT SEND !!!!!!!
pub struct Runner {
    inner: Arc<Inner>,
    state: Arc<Mutex<ThreadState>>,
    cooperative: bool,

    // force Runner to be !Send
    phantom: PhantomData<Rc<()>>,
}

impl Drop for Runner {
    fn drop(&mut self) {
        let mut write_guard = self.inner.threads.threads.write().unwrap();
        let mut guard = self.state.lock().unwrap();

        write_guard.remove(&guard.handle.id());

        if let Some(state) = write_guard.values().next() {
            let mut state = state.lock().unwrap();
            state.woken.extend(guard.woken.drain(..));
            state.pending.extend(guard.pending.drain());
        }
    }
}

impl Runner {
    pub fn tick(&self, waker: Waker) -> bool {
        if self.stopping() {
            return false;
        }

        self.state.lock().unwrap().waker = Some(waker.clone());

        let mut any_polled = false;

        while let Some(task) = self.pop_front_woken() {
            any_polled = true;
            task.poll(Arc::downgrade(&self.state), waker.clone());
            if self.cooperative {
                break;
            }
        }

        any_polled
    }

    fn pop_front_woken(&self) -> Option<Arc<Task>> {
        self.state.lock().unwrap().woken.pop_front()
    }

    pub fn stopping(&self) -> bool {
        self.inner.stopping.load(Ordering::Acquire)
    }

    pub fn park(&self) {
        if !self.steal() {
            self.inner
                .parked
                .lock()
                .unwrap()
                .push_back(std::thread::current());

            std::thread::park();
        }
    }

    fn steal(&self) -> bool {
        let read_guard = self.inner.threads.threads.read().unwrap();
        if read_guard.len() == 1 {
            return false;
        }

        let (last_steal, current_tid) = {
            let mut guard = self.state.lock().unwrap();
            (guard.last_steal.take(), guard.handle.id())
        };

        let (tid, stolen) = match last_steal {
            Some(id) => {
                if read_guard.contains_key(&id) {
                    let opt = read_guard
                        .iter()
                        .chain(read_guard.iter())
                        .filter(|(tid, _)| **tid != current_tid)
                        .skip_while(|(tid, _)| **tid != id)
                        .nth(1);

                    if let Some((tid, thread_state)) = opt {
                        let mut state = thread_state.lock().unwrap();

                        if let Some(v) = state.steal_from() {
                            (*tid, v)
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            None => {
                let opt = read_guard.iter().find(|(tid, _)| **tid != current_tid);

                if let Some((tid, thread_state)) = opt {
                    let mut state = thread_state.lock().unwrap();
                    if let Some(v) = state.steal_from() {
                        (*tid, v)
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        };

        let mut guard = self.state.lock().unwrap();
        for task in &stolen {
            guard.pending.insert(task.task_id, Arc::clone(task));
        }
        guard.woken.extend(stolen);
        guard.last_steal = Some(tid);

        true
    }

    pub fn any_woken(&self) -> bool {
        !self.state.lock().unwrap().woken.is_empty()
    }

    pub fn prune(&self) {
        self.state.lock().unwrap().prune();
    }

    pub fn heuristic_prune(&self) {
        self.state.lock().unwrap().heuristic_prune();
    }

    fn spawn<T: Send + 'static>(
        &self,
        future: impl Future<Output = T> + Send + 'static,
    ) -> JoinHandle<T> {
        self.state.lock().unwrap().spawn(&self.inner, future)
    }

    fn block_on<T: Send + 'static>(
        &self,
        f: impl Future<Output = T> + Send + 'static,
    ) -> Result<T, JoinError> {
        let mut join_handle = self.spawn(f);

        let block_on_waker = Arc::new(BlockOnWaker {
            thread: std::thread::current(),
        })
        .into();

        let mut block_on_context = Context::from_waker(&block_on_waker);

        loop {
            while self.tick(block_on_waker.clone()) {
                if let Poll::Ready(res) = Pin::new(&mut join_handle).poll(&mut block_on_context) {
                    return res;
                }

                self.heuristic_prune();
            }

            if let Poll::Ready(res) = Pin::new(&mut join_handle).poll(&mut block_on_context) {
                return res;
            }

            if self.stopping() {
                return Err(JoinError);
            }

            self.park();
        }
    }
}

impl Default for Executor {
    fn default() -> Self {
        Self::new()
    }
}

impl Executor {
    pub fn new() -> Self {
        Executor {
            inner: Arc::new(Inner {
                task_id_counter: AtomicU64::new(0),
                stopping: AtomicBool::new(false),
                threads: Threads::new(),
                parked: Mutex::new(VecDeque::new()),
            }),
        }
    }

    pub fn into_runner(self) -> Runner {
        self._into_runner(false)
    }

    pub fn into_runner_cooperative(self) -> Runner {
        self._into_runner(true)
    }

    fn _into_runner(self, cooperative: bool) -> Runner {
        let state = ThreadState::new();
        let id = state.handle.id();

        let state = Arc::new(Mutex::new(state));

        {
            let mut guard = self.inner.threads.threads.write().unwrap();
            guard.entry(id).or_insert_with(|| Arc::clone(&state));
        }

        Runner {
            inner: self.inner,
            state,
            cooperative,
            phantom: PhantomData,
        }
    }

    pub fn stop(&self) {
        self.inner.stop();
    }

    pub fn spawn<T: Send + 'static>(
        &self,
        future: impl Future<Output = T> + Send + 'static,
    ) -> JoinHandle<T> {
        let read_guard = self.inner.threads.threads.read().unwrap();
        if read_guard.is_empty() {
            panic!("No running runner");
        }

        let last_spawn = self
            .inner
            .threads
            .last_spawn
            .fetch_add(1, Ordering::Relaxed)
            % read_guard.len();

        let state = read_guard.values().nth(last_spawn).unwrap();
        let mut guard = state.lock().unwrap();
        let res = guard.spawn(&self.inner, future);
        if let Some(waker) = guard.waker.as_ref() {
            waker.wake_by_ref();
        }

        res
    }

    pub fn block_on<T: Send + 'static>(
        &self,
        f: impl Future<Output = T> + Send + 'static,
    ) -> Result<T, JoinError> {
        self.clone().into_runner().block_on(f)
    }

    pub async fn run_with<T: Send + 'static>(
        &self,
        f: impl Future<Output = T> + Send + 'static,
    ) -> Result<T, JoinError> {
        let runner = self.clone().into_runner();

        let handle = self.spawn(f);

        RunWith {
            handle,
            runner,
            cooperative: false,
        }
        .await
    }
}

struct RunWith<T> {
    handle: JoinHandle<T>,
    runner: Runner,
    cooperative: bool,
}

impl Future for Runner {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.as_mut();

        loop {
            while this.tick(cx.waker().clone()) {
                // processing spawned tasks
            }

            if this.stopping() {
                return Poll::Ready(());
            }

            if !this.steal() {
                return Poll::Pending;
            }
        }
    }
}

impl<T> Future for RunWith<T>
where
    T: Send + 'static,
{
    type Output = Result<T, JoinError>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut this = self.as_mut();

        loop {
            while this.runner.tick(cx.waker().clone()) {
                if let Poll::Ready(t) = Pin::new(&mut this.handle).poll(cx) {
                    return Poll::Ready(t);
                }

                if this.cooperative {
                    cx.waker().wake_by_ref();
                    return Poll::Pending;
                }
            }

            if let Poll::Ready(t) = Pin::new(&mut this.handle).poll(cx) {
                return Poll::Ready(t);
            }

            if this.runner.stopping() {
                return Poll::Ready(Err(JoinError));
            }

            if !this.runner.steal() {
                return Poll::Pending;
            }
        }
    }
}
